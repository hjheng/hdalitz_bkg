from PhysicsTools.HepMCCandAlgos.genParticles_cfi import genParticles
from GeneratorInterface.Core.generatorSmeared_cfi import generatorSmeared
from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJetsNoNu
import FWCore.ParameterSet.Config as cms

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *

generator = cms.EDFilter(
    "Pythia8GeneratorFilter",
    pythiaPylistVerbosity=cms.untracked.int32(1),
    filterEfficiency=cms.untracked.double(1.0),
    pythiaHepMCVerbosity=cms.untracked.bool(False),
    comEnergy=cms.double(13000.0),
    crossSection=cms.untracked.double(0.0),
    maxEventsToPrint=cms.untracked.int32(0),
    PythiaParameters=cms.PSet(
        processParameters=cms.vstring(
            "Main:timesAllowErrors = 10000",
            "WeakBosonAndParton:qqbar2gmZg = on",
            "WeakBosonAndParton:qg2gmZq = on",
            "WeakBosonAndParton:fgm2gmZf = on",
            "23:mMin = 0.211316751",
            # "23:mMax = 60.0",
            "23:onMode = off",
            "23:OnIfMatch = 13 -13",
            "PhaseSpace:pTHatMin = 25.",
            "PhaseSpace:mHatMin = 0.211316751",
        ),
        parameterSets=cms.vstring("processParameters"),
    ),
)

# Double EM-enriched filter (not used)
# https://github.com/cms-sw/cmssw/blob/master/GeneratorInterface/Core/src/PythiaHepMCFilterGammaGamma.cc

genParticlesForFilter = cms.EDProducer(
    "GenParticleProducer",
    saveBarCodes=cms.untracked.bool(True),
    src=cms.InputTag("generator", "unsmeared"),
    abortOnUnknownPDGCode=cms.untracked.bool(False),
)

# emenrichingfilter = cms.EDFilter(
#     "EMEnrichingFilter",
#     filterAlgoPSet=cms.PSet(
#         isoGenParETMin=cms.double(20.0),
#         isoGenParConeSize=cms.double(0.1),
#         clusterThreshold=cms.double(20.0),
#         isoConeSize=cms.double(0.2),
#         hOverEMax=cms.double(0.5),
#         tkIsoMax=cms.double(5.0),
#         caloIsoMax=cms.double(10.0),
#         requireTrackMatch=cms.bool(False),
#         genParSource=cms.InputTag("genParticlesForFilter"),
#     ),
# )

# HDalitzmmFilter = cms.EDFilter(
#     "HDalitzMuMuFilter",
#     HepMCProduct=cms.InputTag("generator", "unsmeared"),
#     minLeptonPt=cms.double(3.0),
#     maxAbsLeptonEta=cms.double(3.0),
#     minDimuMass=cms.double(0.211316751),
#     maxDimuMass=cms.double(60.0),
#     minDimuPt=cms.double(25.0)
# )

# HDalitzJetFilter = cms.EDFilter(
#     "HDalitzJetFilter",
#     src=cms.InputTag("ak4GenJets"),
#     minJetPt=cms.double(25.),
#     maxAbsJetEta=cms.double(3.)
# )


# add your filters to this sequence

# ProductionFilterSequence = cms.Sequence(
#     generator * (genParticlesForFilter + emenrichingfilter + HDalitzmmFilter)
# )
# ProductionFilterSequence = cms.Sequence(
#     generator * (genParticlesForFilter + emenrichingfilter)
# )
# ProductionFilterSequence = cms.Sequence(
#     generator * (genParticlesForFilter + HDalitzmmFilter)
# )

# Reference: https://github.com/cms-sw/genproductions/blob/master/python/ThirteenTeV/GJetsVBF/GJets_Mjj-800_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_qCut45_LHE_pythia8_cff.py
# ProductionFilterSequence = cms.Sequence(
#     generator * (genParticlesForFilter +
#                  cms.SequencePlaceholder('randomEngineStateProducer') +
#                  cms.SequencePlaceholder('VtxSmeared') +
#                  generatorSmeared +
#                  genParticles +
#                  genParticlesForJets +
#                  genParticlesForJetsNoNu +
#                  ak4GenJets +
#                  HDalitzJetFilter)
# )
ProductionFilterSequence = cms.Sequence(
    generator * (genParticlesForFilter)
)
